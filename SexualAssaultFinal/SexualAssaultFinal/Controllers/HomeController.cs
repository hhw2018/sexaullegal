﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SexualAssaultFinal.Models;
using SexualAssaultFinal.Helper;

namespace SexualAssaultFinal.Controllers
{
    

    public class HomeController : Controller
    {
        private const double EARTH_RADIUS = 6378137;
        private agencykid db = new agencykid();
        private FAQmodel FAQdb = new FAQmodel();
        private Tipmodel tipdb = new Tipmodel();
        private DistanceModel distancedb = new DistanceModel();


        public ActionResult Tweet()
        {
            List<Tweets> resultlist = new List<Tweets>();
            tweetHelper th = new tweetHelper();
            resultlist = th.serarch();
            ViewBag.result = resultlist.ToList();
            return View();
                }
   

        public ActionResult Index()
        {
            List < Item > newslist = RSSHelper.read("http://www.abc.net.au/news/feed/2006/rss.xml").ToList();
            List<Item> resultlist =new List<Item>();
            ViewBag.Title = "Sexual Assault Awareness";
            for (int i=0;i<3;i++)
            {
            resultlist.Add((Item)newslist.ElementAt(i));

            }
            
            ViewBag.News = resultlist;
            return View();
        }

        public ActionResult Decision()
        {

            return View();
        }
        public ActionResult start()
        {
            return View();

        }

        public ActionResult Search()
        {
            ViewBag.Title = "Search for Agencies";
            return View();
        }
        public ActionResult Statistics()
        {
            ViewBag.Title = "Victoria Statistics";
            return View();
        }
        public ActionResult Disclaimer()
        {
            ViewBag.Title = "Disclaimer";
            return View();
        }
        public ActionResult News()
        {
            ViewBag.listItems = RSSHelper.read("http://www.abc.net.au/news/feed/2006/rss.xml").ToList();
            return View();
        }

        //search for agency
        [HttpPost]
        public ActionResult SearchAgency(string code)
        {
            List<Agency> testlist = new List<Agency>();
            foreach (var item in db.Agency.ToList())
            {
                if (code.Trim().Equals(item.A_pcode.Trim()))
                {
                    Agency t1 = new Agency();
                    t1.A_id = item.A_id;
                    t1.A_name = item.A_name;
                    t1.A_pcode = item.A_pcode;
                    t1.A_contact = item.A_contact;
                    t1.A_Address = item.A_Address;
                    testlist.Add(t1);

                }

            }


            return Json(testlist, JsonRequestBehavior.AllowGet);

        }

        //search cloest court
        [HttpPost]
        public ActionResult SearchCourt(string code)
        {
            List<Court> courtlist = new List<Court>();
            double[] distancearray = new double[14];
            double tempdistance = 0.0000;
            double currentlat = 0.0000;
            double currentlng = 0.0000;
            int count = 0;
            foreach (var item in distancedb.VICPostCode.ToList())
            {
                if (code.Trim().Equals(item.P_postcode.Trim()))
                {
                    currentlat = (double)item.P_lat;
                    currentlng = (double)item.P_lng;
                    break;
                }
            }

            foreach (var item in distancedb.Court.ToList())
            {
                
                Court c1 = new Court();
                double courtlat = (double)item.C_lat;
                double courtlng = (double)item.C_lng;
                c1.C_address = item.C_address;
                c1.C_lat = item.C_lat;
                c1.C_lng = item.C_lng;
                c1.C_name = item.C_name;
                c1.C_suburb = item.C_suburb;
                courtlist.Add(c1);
                distancearray[count] = GetDistance(currentlat, currentlng, courtlat, courtlng);
                if (count == 0)
                {
                    tempdistance = distancearray[0];
                    count++;
                    continue;

                }
                else
                { 
                if (distancearray[count-1]<=distancearray[count])
                {

                    distancearray[count] = tempdistance;
                    courtlist.RemoveAt(1);
                    count++;
                    continue;
                }
                else
                {

                        tempdistance = distancearray[count];
                    courtlist.RemoveAt(0);
                    count++;
                }

                }
            }


            return Json(courtlist, JsonRequestBehavior.AllowGet);

        }

        //search tips
        [HttpPost]
        public ActionResult SearchTips(string s1, string s2, string s3,string s4,string s5)
        {
         

            List<FAQ> testlist = new List<FAQ>();
           foreach(var item in FAQdb.FAQ.ToList()) { 
                if(s1.Trim().Equals(item.F_age.Trim())||s2.Trim().Equals(item.F_SA_Cate.Trim())||s3.Trim().Equals(item.F_time.Trim())||s4.Trim().Equals(item.F_relationship.Trim())||s5.Trim().Equals(item.F_reportTo.Trim()))
                { 
                    FAQ t1 = new FAQ();
                    t1.F_age = item.F_age;
                    t1.F_answer = item.F_answer;
                    t1.F_reportTo = item.F_reportTo;
                    t1.F_question = item.F_question;
                    t1.F_relationship = item.F_relationship;
                    t1.F_SA_Cate = item.F_SA_Cate;
                    t1.F_time = item.F_time;
                    testlist.Add(t1);
                }

            }
            return Json(testlist, JsonRequestBehavior.AllowGet);

        }

        //caculate distance by lng and lat
        private static double GetDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radLat1 = Rad(lat1);
            double radLng1 = Rad(lng1);
            double radLat2 = Rad(lat2);
            double radLng2 = Rad(lng2);
            double a = radLat1 - radLat2;
            double b = radLng1 - radLng2;
            double result = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) + Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2))) * EARTH_RADIUS;
            return result;
        }

      //radius
        private static double Rad(double d)
        {
            return (double)d * Math.PI / 180d;
        }

        public ActionResult Support()
        {
            ViewBag.Title = "Support";
            return View();
        }



    }
}