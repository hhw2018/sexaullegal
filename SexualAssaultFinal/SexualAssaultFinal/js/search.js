﻿
    var tabsSwiper = new Swiper('.swiper-container', {
        speed: 500,
        onSlideChangeStart: function () {
        $(".tabs .active").removeClass('active');
    $(".tabs a").eq(tabsSwiper.activeIndex).addClass('active');
        }
    });

    $(".tabs a").on('touchstart mousedown', function (e) {
        e.preventDefault()
        $(".tabs .active").removeClass('active');
        $(this).addClass('active');
        tabsSwiper.swipeTo($(this).index());
    });

    $(".tabs a").click(function (e) {
        e.preventDefault();
    });


    //<!------------------------------- search for agency------------------------------>
        
            $(document).ready(function () {
        var main = document.getElementById('contain');

        $('#searchagency').click(function () {
            debugger
            var inputcode = document.getElementById("codesearch").value;
            if (inputcode == "" || inputcode.isNaN || inputcode.length != 4) {
                if (inputcode == "") {
                alert("please input a code!")
                    return false;
                }
                else if (typeof (inputcode) != "number" || inputcode.length != 4) {

                alert("please input 4 numbers postcode");
            return false;

                }






            }
            //delete previous list
            if (main.hasChildNodes()) {
                var mainchildren = document.getElementById('contain').childNodes;
                var length = mainchildren.length;
                for (var i = 0; i < length; i++) {
                main.removeChild(mainchildren[0]);
            }
            }
            //post and get value; !!!!!when we publish it , the ajax should have a relative direction that should be like ../search/searchagency
            $.ajax({
                cache: false,
                type: "POST",
                url: "../Home/SearchAgency",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({code: inputcode }),
                success: function (data) {
                    if (data != undefined) {
                $.each(data, function (i, item) {
                    addchild(item.A_name, item.A_pcode, item.A_Address);

                });

            }


                },
                failure: function (erromsg) {
                console.log(erromsg);
            alert(erromsg);
                }

            });
        });

        //to add div for each
        function addchild(title, desc, addr) {
            var childsplit = document.createElement("hr");
            var childsplit2 = document.createElement("br");
            var child1 = document.createElement("div");
            var ccc1 = document.createElement("h4");
            var ccc2 = document.createElement("p");
            var ccc3 = document.createElement("p");
            main.appendChild(child1);
            main.appendChild(childsplit);
            main.appendChild(childsplit2);
            child1.appendChild(ccc1);
            child1.appendChild(ccc2);
            child1.appendChild(ccc3);
            ccc1.innerHTML = "Agency name:" + title;
            ccc2.innerHTML = "Agency postcode:" + desc;
            ccc3.innerHTML = "Agency address:" + addr;
        }


    });


                
                $(document).ready(function () {
        var main = document.getElementById('contain');

        $('#searchcourt').click(function () {
            debugger
                var inputcode = document.getElementById("courtcode").value;
            if (inputcode == "" || inputcode.isNaN || inputcode.length != 4) {

                if (inputcode == "") {
                    alert("please input a code!")
                    return false;
                }
                else if (typeof (inputcode) != "number" || inputcode.length != 4) {

                    alert("please input 4 numbers postcode");
                return false;

                }
            }
            //delete previous list
            if (main.hasChildNodes()) {
                var mainchildren = document.getElementById('contain').childNodes;
                var length = mainchildren.length;
                for (var i = 0; i < length; i++) {
                    main.removeChild(mainchildren[0]);
                }
            }
            //post and get value; !!!!!when we publish it , the ajax should have a relative direction that should be like ../search/searchagency
            $.ajax({
                    cache: false,
                type: "POST",
                url: "../Home/SearchCourt",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({code: inputcode }),
                success: function (data) {
                    if (data != undefined) {
                    $.each(data, function (i, item) {
                        addchild(item.C_name, item.C_address, item.C_suburb);

                    });

                }


                },
                failure: function (erromsg) {
                    console.log(erromsg);
                alert(erromsg);
                }

            });
        });

        //to add div for each
        function addchild(name, addr, sub) {
            var childsplit = document.createElement("hr");
            var childsplit2 = document.createElement("br");
            var child1 = document.createElement("div");
            var ccc1 = document.createElement("h4");
            var ccc2 = document.createElement("p");
            var ccc3 = document.createElement("p");
            main.appendChild(child1);
            main.appendChild(childsplit);
            main.appendChild(childsplit2);
            child1.appendChild(ccc1);
            child1.appendChild(ccc2);
            child1.appendChild(ccc3);
            ccc1.innerHTML = "Court name: " + name;
            ccc2.innerHTML = "Address: " + addr;
            ccc3.innerHTML = "Suburb: " + sub;
        }


    });

            //<!------------------------------- search for tips------------------------------>
                //<script type="text/javascript">
                    $(document).ready(function () {
        var main = document.getElementById('contain');

        $('#searchtip').click(function () {
            var se1index = document.getElementById('s1').value;
            var se2index = document.getElementById('s2').value;
            var se3index = document.getElementById('s3').value;
            var se4index = document.getElementById('s4').value;
            var se5index = document.getElementById('s5').value;
            var se6index = document.getElementById('s6').value;
            var se1 = document.getElementById('s1').options[se1index].text;
            var se2 = document.getElementById('s2').options[se2index].text;
            var se3 = document.getElementById('s3').options[se3index].text;
            var se4 = document.getElementById('s4').options[se4index].text;
            var se5 = document.getElementById('s5').options[se5index].text;
            var se6 = document.getElementById('s6').options[se6index].text;
            //delete previous list
            if (main.hasChildNodes()) {
                var mainchildren = document.getElementById('contain').childNodes;
                var length = mainchildren.length;
                for (var i = 0; i < length; i++) {
                        main.removeChild(mainchildren[0]);
                    }
            }
            //post and get value; !!!!!when we publish it , the ajax should have a relative direction that should be like ../search/searchagency
            $.ajax({
                        cache: false,
                type: "POST",
                url: "../Home/SearchTips",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({s1: se1, s2: se2, s3: se3, s4: se4, s5: se5, s6: se6 }),
                success: function (data) {
                    if (data != undefined) {
                        $.each(data, function (i, item) {
                            addchild(item.T_question, item.T_answer, item.T_SA_Cate);

                        });

                    }
                    else {

                        alert("nothing found!");

                    }


                },
                failure: function (erromsg) {
                        console.log(erromsg);
                    alert(erromsg);
                }

            });
        });

        //to add div for each
        function addchild(question, answer, cate) {
            var childsplit = document.createElement("hr");
            var childsplit2 = document.createElement("br");
            var child1 = document.createElement("div");
            var ccc1 = document.createElement("h4");
            var ccc2 = document.createElement("p");
            var ccc3 = document.createElement("p");
            main.appendChild(child1);
            main.appendChild(childsplit);
            main.appendChild(childsplit2);
            child1.appendChild(ccc1);
            child1.appendChild(ccc2);
            child1.appendChild(ccc3);
            ccc1.innerHTML = "Q: " + question;
            ccc2.innerHTML = "A: " + answer;
            ccc3.innerHTML = "Category: " + cate;
        }


    });


