﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SexualAssaultFinal.Startup))]
namespace SexualAssaultFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
