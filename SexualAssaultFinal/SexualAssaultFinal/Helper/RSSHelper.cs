﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using SexualAssaultFinal.Models;

namespace SexualAssaultFinal.Helper
{
    public class RSSHelper
    {
        public static List<Item> read(string url)
        {
            int emptycount = 0;
            List<Item> listItems = new List<Item>();
            try
            {
                XPathDocument doc = new XPathDocument(url);
                XPathNavigator navigator = doc.CreateNavigator();
                XPathNodeIterator nodes = navigator.Select("//item");
                while (nodes.MoveNext())
                {
                    XPathNavigator node = nodes.Current;
                    if (node.IsEmptyElement)
                    {
                        emptycount++;
                        continue;

                    }
                    else
                    {
                        string cate = "", desc = "", guid = "", link = "", pubdate = "", title = "";
                        if (!node.SelectSingleNode("category").IsEmptyElement)
                            cate = node.SelectSingleNode("category").Value;
                        else
                            cate = "nothing here";
                        if (!node.SelectSingleNode("description").IsEmptyElement)
                            desc = node.SelectSingleNode("description").Value;
                        else
                            desc = "nothing here";
                        if (!node.SelectSingleNode("guid").IsEmptyElement)
                            guid = node.SelectSingleNode("guid").Value;
                        else
                            guid = "nothing here";
                        if (!node.SelectSingleNode("link").IsEmptyElement)
                            link = node.SelectSingleNode("link").Value;
                        else
                            link = "nothing here";
                        if (!node.SelectSingleNode("pubDate").IsEmptyElement)
                            pubdate = node.SelectSingleNode("pubDate").Value;
                        else
                            pubdate = "nothing here";
                        if (!node.SelectSingleNode("title").IsEmptyElement)
                            title = node.SelectSingleNode("title").Value;
                        else
                            title = "nothing here";

                        cate = System.Text.RegularExpressions.Regex.Replace(cate, "<[^>]+>", "");
                        desc = System.Text.RegularExpressions.Regex.Replace(desc, "<[^>]+>", "");
                        guid = System.Text.RegularExpressions.Regex.Replace(guid, "<[^>]+>", "");
                        link = System.Text.RegularExpressions.Regex.Replace(link, "<[^>]+>", "");
                        pubdate = System.Text.RegularExpressions.Regex.Replace(pubdate, "<[^>]+>", "");
                        title = System.Text.RegularExpressions.Regex.Replace(title, "<[^>]+>", "");

                        Item item = new Item();
                        item.Category = cate;
                        item.Description = desc;
                        item.Guid = guid;
                        item.Link = link;
                        item.PubDate = pubdate;
                        item.Title = title;
                        listItems.Add(item);

                    }
                }



            }
            catch (Exception e)
            {



                Console.WriteLine(e.Message);

            }



            return listItems;

        }
    }
}