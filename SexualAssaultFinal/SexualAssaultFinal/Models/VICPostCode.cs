namespace SexualAssaultFinal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VICPostCode")]
    public partial class VICPostCode
    {
        [Key]
        public int P_id { get; set; }

        [StringLength(10)]
        public string P_postcode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? P_lat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? P_lng { get; set; }
    }
}
