namespace SexualAssaultFinal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
   

    public partial class agencykid : DbContext
    {

        public agencykid()
            : base("name=agencykid")
        {
        }

        public virtual DbSet<Agency> Agency { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agency>()
                .Property(e => e.A_name)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.A_pcode)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.A_contact)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.A_Address)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.A_lat)
                .HasPrecision(6, 3);

            modelBuilder.Entity<Agency>()
                .Property(e => e.A_lng)
                .HasPrecision(6, 3);

            modelBuilder.Entity<Agency>()
                .Property(e => e.A_type)
                .IsUnicode(false);
        }
    }
}
