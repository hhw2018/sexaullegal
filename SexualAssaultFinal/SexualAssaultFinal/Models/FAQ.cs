namespace SexualAssaultFinal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FAQ")]
    public partial class FAQ
    {
        [Key]
        public int F_id { get; set; }

        [StringLength(200)]
        public string F_question { get; set; }

        [StringLength(1000)]
        public string F_answer { get; set; }

        [StringLength(200)]
        public string F_age { get; set; }

        [StringLength(200)]
        public string F_SA_Cate { get; set; }

        [StringLength(300)]
        public string F_time { get; set; }

        [StringLength(200)]
        public string F_relationship { get; set; }

        [StringLength(100)]
        public string F_reportTo { get; set; }
    }
}
