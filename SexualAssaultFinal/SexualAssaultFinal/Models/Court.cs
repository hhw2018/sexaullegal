namespace SexualAssaultFinal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Court")]
    public partial class Court
    {
        [Key]
        public int C_id { get; set; }

        [StringLength(100)]
        public string C_name { get; set; }

        [StringLength(200)]
        public string C_address { get; set; }

        [StringLength(100)]
        public string C_suburb { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? C_lat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? C_lng { get; set; }
    }
}
