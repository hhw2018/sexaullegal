namespace SexualAssaultFinal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmergencyAgency")]
    public partial class EmergencyAgency
    {
        [Key]
        public int ea_id { get; set; }

        [StringLength(200)]
        public string ea_name { get; set; }

        [StringLength(500)]
        public string ea_loc { get; set; }

        [StringLength(10)]
        public string ea_postcode { get; set; }

        [StringLength(100)]
        public string ea_contact { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ea_lat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ea_lng { get; set; }
    }
}
