namespace SexualAssaultFinal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Tipmodel : DbContext
    {
        public Tipmodel()
            : base("name=Tipmodel")
        {
        }

        public virtual DbSet<Tip> Tip { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tip>()
                .Property(e => e.T_question)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_answer)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_age)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_SA_Cate)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_time)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_relationship)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_mentalcondition)
                .IsUnicode(false);

            modelBuilder.Entity<Tip>()
                .Property(e => e.T_report_Cate)
                .IsUnicode(false);
        }
    }
}
