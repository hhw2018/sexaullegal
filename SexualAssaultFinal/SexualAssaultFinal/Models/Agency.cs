namespace SexualAssaultFinal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Agency")]
    public partial class Agency
    {
        [Key]
        public int A_id { get; set; }

        [StringLength(100)]
        public string A_name { get; set; }

        [StringLength(10)]
        public string A_pcode { get; set; }

        [StringLength(100)]
        public string A_contact { get; set; }

        [StringLength(500)]
        public string A_Address { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? A_lat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? A_lng { get; set; }

        [StringLength(50)]
        public string A_type { get; set; }
    }
}
