namespace SexualAssaultFinal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DistanceModel : DbContext
    {
        public DistanceModel()
            : base("name=DistanceModel")
        {
        }

        public virtual DbSet<Court> Court { get; set; }
        public virtual DbSet<VICPostCode> VICPostCode { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Court>()
                .Property(e => e.C_name)
                .IsUnicode(false);

            modelBuilder.Entity<Court>()
                .Property(e => e.C_address)
                .IsUnicode(false);

            modelBuilder.Entity<Court>()
                .Property(e => e.C_suburb)
                .IsUnicode(false);

            modelBuilder.Entity<Court>()
                .Property(e => e.C_lat)
                .HasPrecision(6, 4);

            modelBuilder.Entity<Court>()
                .Property(e => e.C_lng)
                .HasPrecision(7, 4);

            modelBuilder.Entity<VICPostCode>()
                .Property(e => e.P_postcode)
                .IsUnicode(false);

            modelBuilder.Entity<VICPostCode>()
                .Property(e => e.P_lat)
                .HasPrecision(6, 4);

            modelBuilder.Entity<VICPostCode>()
                .Property(e => e.P_lng)
                .HasPrecision(7, 4);
        }
    }
}
