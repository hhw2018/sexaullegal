namespace SexualAssaultFinal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FAQmodel : DbContext
    {
        public FAQmodel()
            : base("name=FAQmodel")
        {
        }

        public virtual DbSet<FAQ> FAQ { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_question)
                .IsUnicode(false);

            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_answer)
                .IsUnicode(false);

            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_age)
                .IsUnicode(false);

            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_SA_Cate)
                .IsUnicode(false);

            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_time)
                .IsUnicode(false);

            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_relationship)
                .IsUnicode(false);

            modelBuilder.Entity<FAQ>()
                .Property(e => e.F_reportTo)
                .IsUnicode(false);
        }
    }
}
