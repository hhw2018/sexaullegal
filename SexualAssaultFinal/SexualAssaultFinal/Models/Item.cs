﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SexualAssaultFinal.Models
{
    public class Item
    {
        public string Title { set; get; }

        public string Description { set; get; }
        public string Link { set; get; }
        public string Guid { set; get; }
        public string Category { set; get; }
        public string PubDate { set; get; }
    }
}