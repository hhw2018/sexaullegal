namespace SexualAssaultFinal.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tip")]
    public partial class Tip
    {
        [Key]
        public int T_id { get; set; }

        [StringLength(500)]
        public string T_question { get; set; }

        [StringLength(1500)]
        public string T_answer { get; set; }

        [StringLength(100)]
        public string T_age { get; set; }

        [StringLength(200)]
        public string T_SA_Cate { get; set; }

        [StringLength(200)]
        public string T_time { get; set; }

        [StringLength(50)]
        public string T_relationship { get; set; }

        [StringLength(200)]
        public string T_mentalcondition { get; set; }

        [StringLength(200)]
        public string T_report_Cate { get; set; }
    }
}
