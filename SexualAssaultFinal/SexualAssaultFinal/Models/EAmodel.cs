namespace SexualAssaultFinal.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EAmodel : DbContext
    {
        public EAmodel()
            : base("name=EAmodel2")
        {
        }

        public virtual DbSet<EmergencyAgency> EmergencyAgency { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmergencyAgency>()
                .Property(e => e.ea_name)
                .IsUnicode(false);

            modelBuilder.Entity<EmergencyAgency>()
                .Property(e => e.ea_loc)
                .IsUnicode(false);

            modelBuilder.Entity<EmergencyAgency>()
                .Property(e => e.ea_postcode)
                .IsUnicode(false);

            modelBuilder.Entity<EmergencyAgency>()
                .Property(e => e.ea_contact)
                .IsUnicode(false);

            modelBuilder.Entity<EmergencyAgency>()
                .Property(e => e.ea_lat)
                .HasPrecision(6, 3);

            modelBuilder.Entity<EmergencyAgency>()
                .Property(e => e.ea_lng)
                .HasPrecision(7, 3);
        }
    }
}
