﻿INSERT INTO dbo.Agency
(
    A_name,
    A_pcode,
    A_contact,
    A_Address,
    A_lat,
    A_lng,
    A_type
)
VALUES
(   'xxx',   -- A_name - varchar(100)
    '3154',   -- A_pcode - varchar(10)
    'cccxx',   -- A_contact - varchar(100)
    'ffff',   -- A_Address - varchar(500)
    NULL, -- A_lat - numeric(6, 3)
    NULL, -- A_lng - numeric(6, 3)
    'fsdfs'    -- A_type - varchar(50)
    )